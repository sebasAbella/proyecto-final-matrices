# PROYECTO FINAL MATRICES- UNIVERSIDAD ECCI

Este proyecto fue generado en [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2 ; con intención del desarrollo del pryecto final para la materia de Matematicas Discretas - Universidad ECCI.  

## Development server

* Ejecutar proyecto :

```
Para ejecutar el proyecto se debe tener instalado `Node.js`.
```

```
Instalar dependencias : ejecutar sobre el proyecto el comando `npm install` o ` npm i`.
```

```
Ejecutar con el comando `ng serve` o 'npm start'; el proyecto se ejecuta en http://localhost:4200/#/.
```

* Otros comandos :

```
`ng lint` validar linting del proyecto
```

```
`ng test --code-coverage` validar pruebas unitarias del proyecto
```


## Librerias implementadas

* Material Angular
* Husky
* Moment
* Jasmine y Karma
* commitlint

## Copyright 

* Author: 
- Sebastian Abella Rocha / 89680
- luisa Magaly Achury / 89634
