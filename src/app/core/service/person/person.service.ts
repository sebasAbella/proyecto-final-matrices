import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  httpOptionsEmpty: any;
  constructor(
    private http: HttpClient,
  ) {
    this.httpOptionsEmpty = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  getPerson() {
    return this.http.get(`${environment.api}person`, { responseType: 'json' });
  }

  CreatePerson(objPerson: any) {
    return this.http.post(`${environment.api}person`, objPerson, this.httpOptionsEmpty);
  }

  updatePerson(idPerson: string, objPerson: any) {
    console.log(objPerson);
    return this.http.put(`${environment.api}person/${idPerson}`, objPerson, this.httpOptionsEmpty);
  }

  removePerson(idPerson: string) {
    return this.http.delete(`${environment.api}person/${idPerson}`, this.httpOptionsEmpty);
  }
}
