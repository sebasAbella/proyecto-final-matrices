import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  imgUrl: string;
  constructor() { }

  ngOnInit() {
    this.imgUrl = `${environment.assets}/assets/img/layout/ecci2.png`;
  }

}
