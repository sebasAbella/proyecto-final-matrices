import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PersonService } from 'src/app/core/service/person/person.service';
import * as moment from 'moment';
import { SendInformationService } from 'src/app/core/service/SendInformation/send-information.service';
import { UPDATEDATA } from 'src/app/core/config/constants';

export interface DialogData {
  state: 'create' | 'edit';
  arrayEdit: { fullname: any; birth: any; completed: any; id: any; key: any };
}
@Component({
  selector: 'app-step001',
  templateUrl: './step001.component.html',
  styleUrls: ['./step001.component.css'],
})
export class Step001Component {
  formRegister: FormGroup;
  nameTitle: any;

  constructor() {}
}
