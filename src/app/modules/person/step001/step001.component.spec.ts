import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step001Component } from './step001.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';

describe('Step001Component', () => {
  let component: Step001Component;
  let fixture: ComponentFixture<Step001Component>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Step001Component],
      imports: [
        MatTableModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatPaginatorModule,
        MatIconModule,
        ReactiveFormsModule,
        MatDialogModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      providers: [
        MatDialog,
        { provide: MAT_DIALOG_DATA, useValue: { arrayEdit: { birth: '1999/08/08', fullname: 'test' }, birth: '1999/08/08' } },
        { provide: MatDialogRef, useValue: { arrayEdit: { birth: '1999/08/08', fullname: 'test' }, birth: '1999/08/08' } }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
