import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-step000',
  templateUrl: './step000.component.html',
  styleUrls: ['./step000.component.css'],
})
export class Step000Component implements OnInit {
  formRegister: FormGroup;
  numberMatriz: number;
  matrizCreate: any;
  numRow: any;
  numColumn: any;
  copyMatriz: any;
  typeMatriz: any;
  constructor(public formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
      numMatriz: ['', Validators.required],
    });
  }

  generateTable(state: any = true, random: any = false) {
    if (state) {
      console.log(this.formRegister.value);
      let coutn = 0;
      const obj = [];
      const value = Number(this.formRegister.value.numMatriz);
      while (coutn <= value - 1) {
        obj[coutn] = [];
        coutn++;
      }
      let newCountForeach = 0;
      obj.forEach((it) => {
        let newCount = 1;
        newCountForeach++;
        while (newCount <= obj.length) {
          const id = newCountForeach + ',' + newCount;
          it.push({ id, state: false });
          newCount++;
        }
      });
      this.copyMatriz = obj;
      this.matrizCreate = obj;
      this.numberMatriz = Number(this.formRegister.value.numMatriz);

      setTimeout(() => {
        if (random) {
          this.matrizCreate.forEach((el) => {
            el.forEach((it) => {
              console.log(it.id)
              const random_boolean = Math.random() < 0.7;
              document.getElementById(''+it.id)['checked'] = random_boolean;
            });
          });
        }
      });
    } else {
      this.numberMatriz = 0;
    }
  }

  changeState(e) {
    const pst = e.target.id.split(',')[0] - 1;
    const id = e.target.id;
    this.matrizCreate[pst].forEach((el) => {
      if (el.id === id) {
        el.state = document.getElementById(e.target.id)['checked'];
      }
    });
  }

  generateTotal() {
    const objectTrue = [];
    this.matrizCreate.forEach((el) => {
      el.forEach((it) => {
        if (it.state) {
          objectTrue.push({ id: it.id });
        }
      });
    });
    console.log(objectTrue);
    return objectTrue;
  }

  calculateType() {
    // alert('resultado')
    const status = this.generateTotal();
    let typeMatriz = {
      reflexiva: this.validateReflexiva(status),
      antireflexiva: this.validateAntireflexiva(status),
      simetrica: this.validateSimetrica(status),
      asimetrica: this.validateAsimetrica(status),
      transitiva: this.validateTransitiva(status),
    };
    this.typeMatriz = typeMatriz;
    // valida si es reflexiva
    if (this.validateReflexiva(status)) {
      // typeMatriz.reflexiva = true;
      // alert('Matriz reflexiva');
    }
    if (this.validateAntireflexiva(status)) {
      // typeMatriz.antireflexiva = true;
      // alert('Matriz Antireflexiva');
    }
    if (this.validateAntireflexiva(status)) {
      // typeMatriz.antireflexiva = true;
      // alert('Matriz Antireflexiva');
    }
  }

  validateReflexiva(status: any) {
    let count = 0;
    const objectTrue = [];
    let finalStatus = true;
    this.copyMatriz.forEach((it) => {
      objectTrue.push({ id: it[count].id, state: true });
      count++;
    });
    objectTrue.forEach((refelc) => {
      status.forEach((creat) => {
        if (refelc.id === creat.id) {
          refelc.state = true;
        }
      });
    });
    objectTrue.forEach((it) => {
      if (!it.state) {
        finalStatus = false;
      }
    });
    // console.log('statesss::', objectTrue);
    // console.log('final estado', finalStatus);
    return finalStatus;
  }

  validateAntireflexiva(status: any) {
    let count = 0;
    const objectTrue = [];
    let finalStatus = true;
    this.copyMatriz.forEach((it) => {
      objectTrue.push({ id: it[count].id, state: false });
      count++;
    });
    objectTrue.forEach((refelc) => {
      status.forEach((creat) => {
        if (refelc.id === creat.id) {
          refelc.state = true;
        }
      });
    });
    objectTrue.forEach((it) => {
      if (it.state) {
        finalStatus = false;
      }
    });
    return finalStatus;
  }

  validateSimetrica(status: any) {
    return false;
  }

  validateAsimetrica(status: any) {
    return false;
  }

  validateTransitiva(status: any) {
    return false;
  }

  cleanData() {
    this.numberMatriz = 0;
    this.typeMatriz = false;
  }
  /**
   * funcion para cargar la data cuando la vista este creada
   */
  ngOnInit() {}
}
