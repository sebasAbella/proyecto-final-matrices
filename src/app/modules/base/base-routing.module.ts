import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', loadChildren: '../person/person.module#PersonModule',
      },
    { path: '**', loadChildren: '../forbidden/forbidden.module#ForbiddenModule' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BaseRoutingModule { }
