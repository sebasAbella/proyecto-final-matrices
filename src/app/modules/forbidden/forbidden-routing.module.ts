import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForbiddenComponent } from '../../core/layout/forbidden/forbidden.component';

const routes: Routes = [
  { path: 'forbidden', component: ForbiddenComponent},
  { path: '**', component: ForbiddenComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForbiddenRoutingModule { }
